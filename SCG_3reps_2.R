library(AlphaSimR)

nReps=10 

# Set alternative scenario name
scenarioName = "SCG_3reps_2"

# Loop over replicates
for(REP in 1:nReps){
  
  ## Load data ----
  load(paste0("BURNIN_",REP,".RData"))
  
  # Change scenario
  output$scenario = scenarioName
  
  ## Run alternative breeding program ----
  for(year in 21:nCycles){
    
    # Select new parents in current year before advancing the material
    Parents = c(selectInd(SCG,30,gender="M"),
                selectInd(SCG,20,gender="F"))
    
    
    # Variety Validation Trial 
    VVT = selectFam(MLT2, nFam = 2)
    VVT = selectWithinFam(MLT1, nInd = 1)
    VVT = setPheno(MLT2,
                   varE = plotError,
                   reps = 16*3*4, # 16 locs, 3 reps, 4x plot size
                   p = P[year])
    
    # Multi Location Trial (year 2)
    MLT2 = MLT1
    MLT2 = setPheno(MLT2, 
                    varE = plotError,
                    reps = 12*3*2, # 12 locs, 3 reps, 2x plot size
                    p = P[year])
    
    MLT2@pheno = (MLT1@pheno + MLT2@pheno)/2 # for VVT selection
    
    # Multi Location Trial (year 1)
    MLT1 = selectFam(APT, nFam = 5)
    MLT1 = selectWithinFam(MLT1, nInd = 3)
    MLT1 = setPheno(MLT1, 
                    varE = plotError,
                    reps = 12*3*2, # 12 locs, 3 reps, 2x plot size
                    p = P[year])
    
    # Advanced Progeny Trial
    APT = selectFam(PPT, nFam = 6)
    APT = selectWithinFam(APT, nInd = 4)
    APT = setPheno(APT, 
                   varE = plotError,
                   reps = 3*3*2, # 3 locs, 3 reps, 2x plot size
                   p = P[year])
    
    # Preliminary Progeny Trial
    PPT = selectFam(SCG, nFam = 12)
    PPT = selectWithinFam(PPT, nInd = 5)
    PPT = setPheno(PPT, 
                   varE = plotError,
                   reps = 2*3, # 2 locs, 3 reps
                   p = P[year])
    
    # Second Clonal Generation
    SCG = selectFam(FCG, nFam = 34)  
    SCG = selectWithinFam(SCG, nInd = 6)
    SCG = setPheno(SCG, 
                   varE = plantError,
                   reps = 3*3, # 3 plants, 3 reps
                   p = P[year])
    
    # First Clonal Generation (Tuber Progeny)
    FCG = selectFam(Seed, nFam = 60)
    FCG = selectWithinFam(FCG, nInd = 44)
    FCG = setPheno(FCG, 
                   varE = plantError,
                   reps = 3,  # 3 plants
                   p = P[year])
    
    # Seedling Progeny
    Seed = setPheno(F1, 
                    varE = plantError, 
                    p = P[year])
    
    maleSeed = sample(1:12000, (12000*0.7))
    femaleSeed = subset(c(1:12000), !(c(1:12000) %in% maleSeed))
    Seed@gender[maleSeed] <- "M"
    Seed@gender[femaleSeed] <- "F"
    
    
    # Crossing of selected parents
    F1 = randCross(Parents, 
                   nCrosses = nCross, 
                   nProgeny = nProg,
                   ignoreGender = FALSE,
                   balance = FALSE)
    
    
    
    # Report performance of new parents
    output$mean_prnts[year] = meanG(Parents)
    output$var_prnts[year] = varG(Parents)
    output$genic_prnts[year] = genicVarG(Parents)
    
    # Report performance of new F1s
    output$mean_F1[year] = meanG(F1)
    output$var_F1[year] = varG(F1)
    output$genic_F1[year] = genicVarG(F1)
    
    # Report performance of new FCG
    output$mean_FCG[year] = meanG(FCG)
    output$var_FCG[year] = varG(FCG)
    output$genic_FCG[year] = genicVarG(FCG)
    
    # Report performance of new VVT entries
    output$mean_VVT[year] = meanG(VVT)
    output$var_VVT[year] = varG(VVT)
    output$genic_VVT[year] = genicVarG(VVT)

  }
  
  # Save output
  cat(paste0("saving REP ", REP, "\n"))
  saveRDS(output,paste0(scenarioName,"_",REP,".rds"))
}

rm(list=ls())